# Oatmeal Chocolate Chip Pancakes

[Source](https://www.thehealthymaven.com/fluffiest-oatmeal-chocolate-chip-pancakes/)

- [Oatmeal Chocolate Chip Pancakes](#oatmeal-chocolate-chip-pancakes)
  - [Ingredients](#ingredients)
  - [Directions](#directions)

## Ingredients

- 2 cups oats
- 2 tsp baking powder
- 1/4 tsp sea salt
- 1/2 tsp cinnamon
- 3/4 cup (184 grams) plain greek yogurt
- 1 cup milk
- 1 egg
- 1 tsp vanilla extract
- 1/3 cup mini chocolate chips

## Directions

Put everything besides the chocolate chips in the Blendtec. Cook on the griddle, adding chocolate chips after pouring batter.

![Chocolate Pancakes](https://thumbnails-photos.amazon.com/v1/thumbnail/Z7OcMTZpQL2Eg0m6_vf2jg?viewBox=524%2C698&ownerId=AQX5UU8XHTLBL&groupShareToken=zq7ClhgJSGmPuh5OMUrS0A.jJ970sAd9VCq9lHWmFMAmD)
